# Serverless Demo

This repository demonstrates how to configure OpenShift to run serverless workloads based on events backed by Kafka topics

## Environment

- OpenShift 3.7.21
- Serverless Operator 1.16.0
- AMQ Streams Operator 1.7.2

## Checking requirements

<a href='didact://?commandId=vscode.didact.validateAllRequirements' title='Validate all requirements!'><button>Validate all Requirements at Once!</button></a>

**OpenShift CLI ("oc")**

The OpenShift CLI tool ("oc") will be used to interact with the OpenShift cluster.

[Check if the OpenShift CLI ("oc") is installed](didact://?commandId=vscode.didact.cliCommandSuccessful&text=oc-requirements-status$$oc%20help&completion=oc%20is%20availability "Tests to see if `oc help` returns a 0 return code"){.didact}

*Status: unknown*{#oc-requirements-status}

**Knative CLI ("kn")**

The Knative CLI tool ("kn") will be used to interact with the OpenShift cluster.

[Check if the Knative CLI ("kn") is installed](didact://?commandId=vscode.didact.cliCommandSuccessful&text=kn-requirements-status$$kn%20help&completion=kn%20is%20availability "Tests to see if `kn help` returns a 0 return code"){.didact}

*Status: unknown*{#kn-requirements-status}


**Connection to an OpenShift cluster**

You need to connect to an OpenShift cluster in order to run the examples.

[Check if you're connected to an OpenShift cluster](didact://?commandId=vscode.didact.requirementCheck&text=cluster-requirements-status$$oc%20get%20project$$NAME&completion=OpenShift%20is%20connected. "Tests to see if `kamel version` returns a result"){.didact}

*Status: unknown*{#cluster-requirements-status}

## Install Serverless Operator

    oc create namespace openshift-serverless

 ([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20create%20namespace%20openshift-serverless))

    oc apply -f openshift/serverless-group.yaml 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20openshift/serverless-group.yaml))

    oc apply -f openshift/serverless-subscription.yaml 

([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20openshift/serverless-subscription.yaml))

## Enable Knative Serving and Eventing

Once the Serverless Operator has finished its installation, you must enable Knative Serving and Eventing by running this commands:

    oc apply -f openshift/knative-eventing.yaml 

([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20openshift/knative-eventing.yaml))

    oc apply -f openshift/knative-serving.yaml 

([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20openshift/knative-serving.yaml))

## Install AMQ Streams

First we need to install the AMQ Streams operator. In this demo I'm enabling it to the whole cluster:

    oc apply -f openshift/amq-streams-subscription.yaml 

([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20openshift/amq-streams-subscription.yaml))

Then you can create the project to host your Kafka cluster and the topic that will be used:

    oc new-project kafka 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20new-project%20kafka))

    oc apply -f openshift/kafka-cluster.yaml 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20openshift/kafka-cluster.yaml))

    oc apply -f openshift/kafka-topic.yaml 

([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20openshift/kafka-topic.yaml))

## Enable Knative Kafka

To create KafkaSource we need to enable KnativeKafka first:

    oc apply -f openshift/knative-kafka.yaml 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20openshift/knative-kafka.yaml))

## Create Knative Service

Create the project to host the applications

    oc new-project serverless-demo 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20new-project%20serverless-demo))

Create the Knative service to sink the messages

    kn service create eventinghello --namespace serverless-demo --concurrency-limit=1 --image=quay.io/rhdevelopers/eventinghello:0.0.2 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$kn%20service%20create%20eventinghello%20--namespace%20serverless-demo%20--concurrency-limit=1%20--image=quay.io/rhdevelopers/eventinghello:0.0.2))


## Create Source to Sink

    oc apply -f  openshift/mykafka-source.yaml 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20apply%20-f%20%20openshift/mykafka-source.yaml))

## Test Your Installation

### Run The Message Producer Application

    oc -n kafka run kafka-spammer --image=quay.io/rhdevelopers/kafkaspammer:1.0.2 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20-n%20kafka%20run%20kafka-spammer%20--image=quay.io/rhdevelopers/kafkaspammer:1.0.2))

### Create Load

You can send simultaneous messages to force the service to scale

    oc -n kafka exec -it kafka-spammer -- curl localhost:8080/5 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$oc%20-n%20kafka%20exec%20-it%20kafka-spammer%20--%20curl%20localhost:8080/5))


### Check Service Scaling

    watch oc get pods -n serverless-demo 
    
([^ execute](didact://?commandId=vscode.didact.sendNamedTerminalAString&text=newTerminal$$watch%20oc%20get%20pods%20-n%20serverless-demo))
