# Serverless Demo

This repository demonstrates how to configure OpenShift to run serverless workloads based on events backed by Kafka topics

## Environment

- OpenShift 3.7.21
- Serverless Operator 1.16.0
- AMQ Streams Operator 1.7.2

## Install Serverless Operator

    oc create namespace openshift-serverless

    oc apply -f openshift/serverless-group.yaml

    oc apply -f openshift/serverless-subscription.yaml

## Enable Knative Serving and Eventing

Once the Serverless Operator has finished its installation, you must enable Knative Serving and Eventing by running this commands:

    oc apply -f openshift/knative-eventing.yaml

    oc apply -f openshift/knative-serving.yaml

## Install AMQ Streams

First we need to install the AMQ Streams operator. In this demo I'm enabling it to the whole cluster:

    oc apply -f openshift/amq-streams-subscription.yaml

Then you can create the project to host your Kafka cluster and the topic that will be used:

    oc new-project kafka

    oc apply -f openshift/kafka-cluster.yaml

    oc apply -f openshift/kafka-topic.yaml

## Enable Knative Kafka

To create KafkaSource we need to enable KnativeKafka first:

    oc apply -f openshift/knative-kafka.yaml

## Create Knative Service

Create the project to host the applications

    oc new-project serverless-demo

Create the Knative service to sink the messages

    kn service create eventinghello --namespace serverless-demo \
    --concurrency-limit=1 --image=quay.io/rhdevelopers/eventinghello:0.0.2


## Create Source to Sink

    oc apply -f  openshift/mykafka-source.yaml

## Test Your Installation

### Run The Message Producer Application

    oc -n kafka run kafka-spammer --image=quay.io/rhdevelopers/kafkaspammer:1.0.2

### Create Load

You can send simultaneous messages to force the service to scale

    oc -n kafka exec -it kafka-spammer -- curl localhost:8080/5


### Check Service Scaling

    watch oc get pods -n serverless-demo
